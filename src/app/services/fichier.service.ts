import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FichierService {
  private baseUrl = 'http://localhost:8080/dematerialisation/fichier';
  private baseUrl2 = 'http://localhost:8080/dematerialisation/fichier/download';
  constructor(private http: HttpClient) { }

  getFichierList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getFichier(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  upload(file: FormData): Observable<any> {
    return this.http.post(`${this.baseUrl}`, file);
  }

  deleteUtilisateurs(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  downloadFile(id: number): Observable<Blob>{		
		return this.http.get(`${this.baseUrl2}/${id}`,{ responseType: 'blob' });
   }

   updateFichiers(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }
}
