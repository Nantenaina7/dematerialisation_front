import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Fichier } from 'src/app/classes/fichier';
import { FichierService } from 'src/app/services/fichier.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fichier-liste',
  templateUrl: './fichier-liste.component.html',
  styleUrls: ['./fichier-liste.component.css']
})
export class FichierListeComponent implements OnInit {
  fichier: Observable<Fichier[]>;
  fileToUpload: File = null;

  constructor(private fichierService: FichierService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.fichier = this.fichierService.getFichierList();
  }

  deleteFichier(id: number) {
    this.fichierService.deleteUtilisateurs(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  detail(id : number){
    this.router.navigate(['/fichier', id]);
  }

  download2(id : number) {
    this.fichierService.downloadFile(id).subscribe(
      data =>{
        console.log(data);
        var blob = new Blob([data],{type: 'application/pdf'});
        console.log(blob);
        var downloadURL = window.URL.createObjectURL(blob);
        var link = document.createElement('a');
        link.href = downloadURL;
        link.download = ".pdf";
        link.click();
      }
    )
  }

  fileProgress(fileInput: any) {
    this.fileToUpload = <File>fileInput.target.files[0];
    console.log(this.fileToUpload);
  }

  onSubmit() {
    const formData = new FormData();
      formData.append('file', this.fileToUpload);
      this.fichierService.upload(formData).subscribe(
        data =>{
          this.reloadData();
        }
      )
  }

  updateFichier(id: number) {
    this.router.navigate(['fichier/update', id]);
  }
}
