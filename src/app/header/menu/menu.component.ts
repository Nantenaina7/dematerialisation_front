import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

   afficherListUser(){
     this.router.navigate(['utilisateurs']);
   }

  ajoutUser(){
    this.router.navigate(['utilisateurs/add']);
  }

  fichier(){
    this.router.navigate(['/fichier']);
  }

  deconnexion(){
    this.router.navigate(['/']);
  }
}
