import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUtilisateursComponent } from './pages/create-utilisateurs/create-utilisateurs.component';
import { UpdateUtilisateursComponent } from './pages/update-utilisateurs/update-utilisateurs.component';
import { UtilisateursDetailComponent } from './pages/utilisateurs-detail/utilisateurs-detail.component';
import { UtilisateursListComponent } from './pages/utilisateurs-list/utilisateurs-list.component';
import { LoginComponent } from './pages/login/login.component';
import { FichierListeComponent } from './pages/fichier-liste/fichier-liste.component';
import { FichierDetailComponent } from './pages/fichier-detail/fichier-detail.component';
import { FichierUpdateComponent } from './pages/fichier-update/fichier-update.component';


const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'utilisateurs', component: UtilisateursListComponent},
  { path: 'utilisateurs/add', component: CreateUtilisateursComponent },
  { path: 'update/:id', component: UpdateUtilisateursComponent },
  { path: 'details/:id', component: UtilisateursDetailComponent },
  { path: 'fichier', component: FichierListeComponent },
  { path: 'fichier/:id', component: FichierDetailComponent },
  { path: 'fichier/update/:id', component: FichierUpdateComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }