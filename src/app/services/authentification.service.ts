import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  // BASE_PATH: 'http://localhost:8080'
  USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

  public user: String;
  public password: String;

  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  login(user : string, password : string): Observable<any> {
    let params = new HttpParams();
    params = params.append('user', user);
    params = params.append('password', password);
    return this.http.post(`${this.baseUrl}`,params);
  }

  authenticationService(user : string, password : string) {
    return this.http.post(`${this.baseUrl}`,
      { headers: { authorization: this.createBasicAuthToken(user, password) } }).pipe(map((res) => {
        this.user = user;
        this.password = password;
        this.registerSuccessfulLogin(user, password);
      }));
  }

  createBasicAuthToken(username: String, password: String) {
    return 'Basic ' + window.btoa(username + ":" + password)
  }

  registerSuccessfulLogin(user, password) {
    sessionStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, user)
  }

  logout() {
    sessionStorage.removeItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
    this.user = null;
    this.password = null;
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return false
    return true
  }

  getLoggedInUserName() {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return ''
    return user
  }
}
