import { Component, OnInit } from '@angular/core';
import { Fichier } from 'src/app/classes/fichier';
import { ActivatedRoute, Router } from '@angular/router';
import { FichierService } from 'src/app/services/fichier.service';

@Component({
  selector: 'app-fichier-update',
  templateUrl: './fichier-update.component.html',
  styleUrls: ['./fichier-update.component.css']
})
export class FichierUpdateComponent implements OnInit {

  id: number;
  fichier: Fichier;

  constructor(private route: ActivatedRoute,private router: Router,
    private fichierService: FichierService) { }

  ngOnInit() {
    this.fichier = new Fichier();

    this.id = this.route.snapshot.params['id'];
    
    this.fichierService.getFichier(this.id)
      .subscribe(data => {
        console.log(data)
        this.fichier = data;
      }, error => console.log(error));
  }

  updateFichier() {
    this.fichierService.updateFichiers(this.id, this.fichier)
      .subscribe(data => console.log(data), error => console.log(error));
    this.fichier = new Fichier();
    this.gotoList();
  }

  onSubmit() {
    this.updateFichier();    
  }

  gotoList() {
    this.router.navigate(['/fichier']);
  }

  list(){
    this.router.navigate(['/fichier']);
  }

}
