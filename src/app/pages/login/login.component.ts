import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { Utilisateurs } from 'src/app/classes/utilisateurs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mail: string;
  utilisateurs: Utilisateurs;
  password : string;
  errorMessage = 'Invalid Credentials';
  successMessage: string;
  invalidLogin = false;
  loginSuccess = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authentificationService: AuthentificationService) {   }

  ngOnInit() {
  }

  handleLogin() {
    this.authentificationService.login(this.mail, this.password).subscribe(data => {
      console.log(data)
      console.log(this.mail)
      console.log(this.password)
      this.utilisateurs = data;
      if(data == null){
        this.invalidLogin = true;
        this.loginSuccess = false;
      }
      else{
        this.invalidLogin = false;
        this.loginSuccess = true;
        this.successMessage = 'Login Successful.';
        this.router.navigate(['utilisateurs']);
      }
    }, error => console.log(error));
    
  }
}
