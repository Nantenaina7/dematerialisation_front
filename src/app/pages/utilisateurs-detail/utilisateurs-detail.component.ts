import { Component, OnInit } from '@angular/core';
import { Utilisateurs } from 'src/app/utilisateurs';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilisateursService } from 'src/app/services/utilisateurs.service';

@Component({
  selector: 'app-utilisateurs-detail',
  templateUrl: './utilisateurs-detail.component.html',
  styleUrls: ['./utilisateurs-detail.component.css']
})
export class UtilisateursDetailComponent implements OnInit {
  id: number;
  utilisateurs: Utilisateurs;

  constructor(private route: ActivatedRoute,private router: Router,
    private utilisateurService: UtilisateursService) { }

  ngOnInit() {
    this.utilisateurs = new Utilisateurs();

    this.id = this.route.snapshot.params['id'];
    
    this.utilisateurService.getUtilisateurs(this.id)
      .subscribe(data => {
        console.log(data)
        this.utilisateurs = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['/utilisateurs']);
  }
}
