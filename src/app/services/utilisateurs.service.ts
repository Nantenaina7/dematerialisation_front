import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilisateursService {
  private baseUrl = 'http://localhost:8080/dematerialisation/utilisateurs';

  constructor(private http: HttpClient) { }

  getUtilisateurs(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createutilisateurs(employee: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, employee);
  }

  updateUtilisateurs(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteUtilisateurs(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getUtilisateursList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
