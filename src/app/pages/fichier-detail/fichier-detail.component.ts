import { Component, OnInit } from '@angular/core';
import { Fichier } from 'src/app/classes/fichier';
import { ActivatedRoute, Router } from '@angular/router';
import { FichierService } from 'src/app/services/fichier.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-fichier-detail',
  templateUrl: './fichier-detail.component.html',
  styleUrls: ['./fichier-detail.component.css']
})
export class FichierDetailComponent implements OnInit {
  lien: String;
  id: number;
  fichier: Fichier;
  content : any;

  constructor(private route: ActivatedRoute,private router: Router,
    private fichierService: FichierService,private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.fichier = new Fichier();

    this.id = this.route.snapshot.params['id'];
    
    this.fichierService.downloadFile(this.id)
      .subscribe(response => {
        var file = new Blob([response],{type : 'application/pdf'});
        console.log(URL.createObjectURL(file));     
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['/']);
  }

}
