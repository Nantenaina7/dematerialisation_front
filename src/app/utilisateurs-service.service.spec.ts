import { TestBed } from '@angular/core/testing';

import { UtilisateursServiceService } from './utilisateurs-service.service';

describe('UtilisateursServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilisateursServiceService = TestBed.get(UtilisateursServiceService);
    expect(service).toBeTruthy();
  });
});
