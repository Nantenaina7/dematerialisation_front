import { Component, OnInit } from '@angular/core';
import { Utilisateurs } from 'src/app/utilisateurs';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilisateursService } from 'src/app/services/utilisateurs.service';

@Component({
  selector: 'app-update-utilisateurs',
  templateUrl: './update-utilisateurs.component.html',
  styleUrls: ['./update-utilisateurs.component.css']
})
export class UpdateUtilisateursComponent implements OnInit {
  id: number;
  utilisateurs: Utilisateurs;

  constructor(private route: ActivatedRoute,private router: Router,
    private utilisateurService: UtilisateursService) { }

  ngOnInit() {
    this.utilisateurs = new Utilisateurs();

    this.id = this.route.snapshot.params['id'];
    
    this.utilisateurService.getUtilisateurs(this.id)
      .subscribe(data => {
        console.log(data)
        this.utilisateurs = data;
      }, error => console.log(error));
  }

  updateUtilisateurs() {
    this.utilisateurService.updateUtilisateurs(this.id, this.utilisateurs)
      .subscribe(data => console.log(data), error => console.log(error));
    this.utilisateurs = new Utilisateurs();
    this.gotoList();
  }

  onSubmit() {
    this.updateUtilisateurs();    
  }

  gotoList() {
    this.router.navigate(['/utilisateurs']);
  }

  list(){
    this.router.navigate(['/utilisateurs']);
  }
}
