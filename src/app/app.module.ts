import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CreateUtilisateursComponent } from './pages/create-utilisateurs/create-utilisateurs.component';
import { UtilisateursDetailComponent } from './pages/utilisateurs-detail/utilisateurs-detail.component';
import { UpdateUtilisateursComponent } from './pages/update-utilisateurs/update-utilisateurs.component';
import { UtilisateursListComponent } from './pages/utilisateurs-list/utilisateurs-list.component';
import { LoginComponent } from './pages/login/login.component';
import { MenuComponent } from './header/menu/menu.component';
import { FichierListeComponent } from './pages/fichier-liste/fichier-liste.component';
import { FichierDetailComponent } from './pages/fichier-detail/fichier-detail.component';
import { FichierUpdateComponent } from './pages/fichier-update/fichier-update.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { HttpInterceptorServiceService } from './services/http-interceptor-service.service';

@NgModule({
  declarations: [
    AppComponent,
    CreateUtilisateursComponent,
    UtilisateursDetailComponent,
    UpdateUtilisateursComponent,
    UtilisateursListComponent,
    LoginComponent,
    MenuComponent,
    FichierListeComponent,
    FichierDetailComponent,
    FichierUpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxExtendedPdfViewerModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorServiceService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
