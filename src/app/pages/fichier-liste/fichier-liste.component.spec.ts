import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichierListeComponent } from './fichier-liste.component';

describe('FichierListeComponent', () => {
  let component: FichierListeComponent;
  let fixture: ComponentFixture<FichierListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichierListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichierListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
