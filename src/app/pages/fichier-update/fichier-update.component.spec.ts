import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichierUpdateComponent } from './fichier-update.component';

describe('FichierUpdateComponent', () => {
  let component: FichierUpdateComponent;
  let fixture: ComponentFixture<FichierUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichierUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichierUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
