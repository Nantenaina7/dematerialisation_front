import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Utilisateurs } from 'src/app/utilisateurs';
import { UtilisateursService } from 'src/app/services/utilisateurs.service';

@Component({
  selector: 'app-utilisateurs-list',
  templateUrl: './utilisateurs-list.component.html',
  styleUrls: ['./utilisateurs-list.component.css']
})
export class UtilisateursListComponent implements OnInit {
  utilisateurs: Observable<Utilisateurs[]>;

  constructor(private utilisateurService: UtilisateursService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.utilisateurs = this.utilisateurService.getUtilisateursList();
  }

  deleteUtilisateurs(id: number) {
    this.utilisateurService.deleteUtilisateurs(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  utilisateursDetails(id: number){
    this.router.navigate(['details', id]);
  }

  updateUtilisateurs(id: number) {
    this.router.navigate(['update', id]);
  }
}
