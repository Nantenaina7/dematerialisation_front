import { Component, OnInit } from '@angular/core';
import { Utilisateurs } from 'src/app/utilisateurs';
import { UtilisateursService } from 'src/app/services/utilisateurs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-utilisateurs',
  templateUrl: './create-utilisateurs.component.html',
  styleUrls: ['./create-utilisateurs.component.css']
})
export class CreateUtilisateursComponent implements OnInit {
  utilisateurs: Utilisateurs = new Utilisateurs();
  submitted = false;

  constructor(private utilisateurService: UtilisateursService,
    private router: Router) { }

  ngOnInit() {
  }

  newUtilisateurs(): void {
    this.submitted = false;
    this.utilisateurs = new Utilisateurs();
  }

  save() {
    this.utilisateurService.createutilisateurs(this.utilisateurs)
      .subscribe(data => console.log(data), error => console.log(error));
    this.utilisateurs = new Utilisateurs();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  gotoList() {
    this.router.navigate(['/utilisateurs']);
  }

  list(){
    this.router.navigate(['/utilisateurs']);
  }
}
